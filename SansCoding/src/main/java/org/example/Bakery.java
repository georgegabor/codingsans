package org.example;

import java.util.List;

public class Bakery {

  static class Info {
    String name;
    String amount;

    public String getName() {
      return name;
    }

    public Info setName(String name) {
      this.name = name;
      return this;
    }

    public String getAmount() {
      return amount;
    }

    public Info setAmount(String amount) {
      this.amount = amount;
      return this;
    }

    @Override
    public String toString() {
      return "\n{\n" + "\"name\":\"" + name + "\"" + ",\n\"amount\":" + amount + "\n}\n";
    }
  }

  static class InfoWithPrice extends Info {
    int price;

    public int getPrice() {
      return price;
    }

    public InfoWithPrice setPrice(int price) {
      this.price = price;
      return this;
    }

    @Override
    public String toString() {
      return "InfoWithPrice{" + "price=" + price + "} " + super.toString();
    }
  }

  static class Recipe {
    String name;
    String price;
    boolean lactoseFree;
    boolean glutenFree;
    List<Info> ingredients;

    public String getName() {
      return name;
    }

    public Recipe setName(String name) {
      this.name = name;
      return this;
    }

    public String getPrice() {
      return price;
    }

    public Recipe setPrice(String price) {
      this.price = price;
      return this;
    }

    public boolean isLactoseFree() {
      return lactoseFree;
    }

    public Recipe setLactoseFree(boolean lactoseFree) {
      this.lactoseFree = lactoseFree;
      return this;
    }

    public boolean isGlutenFree() {
      return glutenFree;
    }

    public Recipe setGlutenFree(boolean glutenFree) {
      this.glutenFree = glutenFree;
      return this;
    }

    public List<Info> getIngredients() {
      return ingredients;
    }

    public Recipe setIngredients(List<Info> ingredients) {
      this.ingredients = ingredients;
      return this;
    }

    @Override
    public String toString() {
      return "Recipe{"
          + "name='"
          + name
          + '\''
          + ", price='"
          + price
          + '\''
          + ", lactoseFree="
          + lactoseFree
          + ", glutenFree="
          + glutenFree
          + ", ingredients="
          + ingredients
          + '}';
    }
  }

  List<Recipe> recipes;
  List<Info> inventory;
  List<Info> salesOfLastWeek;
  List<InfoWithPrice> wholesalePrices;

  public List<Recipe> getRecipes() {
    return recipes;
  }

  public Bakery setRecipes(List<Recipe> recipes) {
    this.recipes = recipes;
    return this;
  }

  public List<Info> getInventory() {
    return inventory;
  }

  public Bakery setInventory(List<Info> inventory) {
    this.inventory = inventory;
    return this;
  }

  public List<Info> getSalesOfLastWeek() {
    return salesOfLastWeek;
  }

  public Bakery setSalesOfLastWeek(List<Info> salesOfLastWeek) {
    this.salesOfLastWeek = salesOfLastWeek;
    return this;
  }

  public List<InfoWithPrice> getWholesalePrices() {
    return wholesalePrices;
  }

  public Bakery setWholesalePrices(List<InfoWithPrice> wholesalePrices) {
    this.wholesalePrices = wholesalePrices;
    return this;
  }

  @Override
  public String toString() {
    return "Bakery{"
        + "recipes="
        + recipes
        + ", inventory="
        + inventory
        + ", salesOfLastWeek="
        + salesOfLastWeek
        + ", wholesalePrices="
        + wholesalePrices
        + '}';
  }
}
