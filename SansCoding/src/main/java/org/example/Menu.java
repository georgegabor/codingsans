package org.example;

import java.util.ArrayList;
import java.util.List;

public class Menu {

  static class Info {
    String name;
    String price;

    public String getName() {
      return name;
    }

    public Info setName(String name) {
      this.name = name;
      return this;
    }

    public String getPrice() {
      return price;
    }

    public Info setPrice(String price) {
      this.price = price;
      return this;
    }

    @Override
    public String toString() {
      return "\n{\n" + "\"name\":\"" + name + "\",\n" + "\"price\":\"" + price + "\"\n" + "}";
    }
  }

  List<Info> glutenFree = new ArrayList<>();
  List<Info> lactoseFree = new ArrayList<>();
  List<Info> lactoseAndGlutenFree = new ArrayList<>();

  public List<Info> getGlutenFree() {
    return glutenFree;
  }

  public Menu setGlutenFree(List<Info> glutenFree) {
    this.glutenFree = glutenFree;
    return this;
  }

  public List<Info> getLactoseFree() {
    return lactoseFree;
  }

  public Menu setLactoseFree(List<Info> lactoseFree) {
    this.lactoseFree = lactoseFree;
    return this;
  }

  public List<Info> getLactoseAndGlutenFree() {
    return lactoseAndGlutenFree;
  }

  public Menu setLactoseAndGlutenFree(List<Info> lactoseAndGlutenFree) {
    this.lactoseAndGlutenFree = lactoseAndGlutenFree;
    return this;
  }

  @Override
  public String toString() {
    return "{\n"
        + "\"glutenFree\":"
        + glutenFree
        + ",\n \"lactoseFree\":"
        + lactoseFree
        + ",\n \"lactoseAndGlutenFree\":"
        + lactoseAndGlutenFree
        + "\n\n}";
  }
}
