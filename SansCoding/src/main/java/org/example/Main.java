package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.json.JSONException;

class Main {
    public static void main(String[] args) throws IOException, URISyntaxException {
        Bakery bakery =
            readFromJsonMapToObjectFailOnException("/bakery.json", Bakery.class);

        //    int result1 = calculateLastWeeksSales(bakery);
        //    System.out.println("result1 = " + result1);

        //    Menu result2 = calculateMenu(bakery);
        //    System.out.println("result2 = " + result2);

        //        int result3 = calculateLastWeeksProfit(bakery);
        //        System.out.println("result3 = " + result3);

        List<Bakery.Info> result4 = calculateHowManyEachRecipeCouldBeMade(bakery);
        System.out.println("result4 = " + result4);


    }
    public static <T> T readFromJsonMapToObjectFailOnException(String JSONFile, Class<T> leClass) {
        String JSONContent = null;
        try {
            JSONContent =
                new String(Files.readAllBytes(Paths.get(
                    Objects.requireNonNull(Main.class.getResource(JSONFile)).toURI())));
        } catch (Exception e) {
//            Assertions.fail("Cannot load test data from " + JSONFile);
            e.printStackTrace();
        }
        T result = null;
        try {
            result = new ObjectMapper().readValue(JSONContent, leClass);
        } catch (JsonProcessingException e) {
//            Assertions.fail("Exception while deserializing test data " + e.getMessage());
            e.printStackTrace();
        }
        return result;
    }

    private static int calculateLastWeeksSales(Bakery bakery) {
        int total = 0;

        for (Bakery.Info sale : bakery.getSalesOfLastWeek()) {
            for (Bakery.Recipe recipe : bakery.getRecipes()) {
                if (sale.getName().equalsIgnoreCase(recipe.getName())) {
                    int price = Integer.parseInt(recipe.getPrice().split(" ")[0]);
                    int amount = Integer.parseInt(sale.getAmount());
                    total += amount * price;
                }
            }
        }

        return total;
    }

    private static Menu calculateMenu(Bakery bakery) {
        Menu menu = new Menu();

        for (Bakery.Recipe recipe : bakery.getRecipes()) {
            if (recipe.isGlutenFree() && recipe.isLactoseFree()) {
                Menu.Info i = new Menu.Info();
                i.setName(recipe.getName());
                i.setPrice(recipe.getPrice());
                menu.getLactoseAndGlutenFree().add(i);
            } else if (recipe.isGlutenFree()) {
                Menu.Info i = new Menu.Info();
                i.setName(recipe.getName());
                i.setPrice(recipe.getPrice());
                menu.getGlutenFree().add(i);
            } else if (recipe.isLactoseFree()) {
                Menu.Info i = new Menu.Info();
                i.setName(recipe.getName());
                i.setPrice(recipe.getPrice());
                menu.getLactoseFree().add(i);
            }
        }

        return menu;
    }

    private static int calculateLastWeeksProfit(Bakery bakery) {
        int total = 0;

        for (Bakery.Info sale : bakery.getSalesOfLastWeek()) {
            for (Bakery.Recipe recipe : bakery.getRecipes()) {
                if (sale.getName().equalsIgnoreCase(recipe.getName())) {
                    int profit = calculateProfitOfSingleRecipe(recipe, bakery.getWholesalePrices());
                    int amount = Integer.parseInt(sale.getAmount());
                    total += amount * profit;
                }
            }
        }

        return total;
    }

    private static int calculateProfitOfSingleRecipe(
            Bakery.Recipe recipe, List<Bakery.InfoWithPrice> wholesalePrices) {
        int totalPriceOfRecipe = 0;

        for (Bakery.Info ingredient : recipe.getIngredients()) {
            for (Bakery.InfoWithPrice wholesalePrice : wholesalePrices) {
                if (ingredient.getName().equalsIgnoreCase(wholesalePrice.getName())) {
                    int priceOfUnit =
                            (wholesalePrice.getPrice()
                                    / Integer.parseInt(wholesalePrice.getAmount().split(" ")[0]));

                    if (ingredient.getName().equalsIgnoreCase("egg")) {
                        totalPriceOfRecipe +=
                                priceOfUnit * Integer.parseInt(ingredient.getAmount().split(" ")[0]);
                    } else {
                        totalPriceOfRecipe +=
                                (priceOfUnit * Integer.parseInt(ingredient.getAmount().split(" ")[0])) / 1000;
                    }
                }
            }
        }

        return Integer.parseInt(recipe.getPrice().split(" ")[0]) - totalPriceOfRecipe;
    }

    private static List<Bakery.Info> calculateHowManyEachRecipeCouldBeMade(Bakery bakery) {
        List<Bakery.Info> recipeList = new ArrayList<>();
        // Find the min of each ingredient
        for (Bakery.Recipe recipe : bakery.getRecipes()) {
            recipeList.add(calculateSingleRecipe(recipe, bakery.getInventory()));
        }
        // Sort by name
        Collator coll = Collator.getInstance(new Locale("hu","HU"));
        recipeList.sort(Comparator.comparing(Bakery.Info::getName, coll));
        return recipeList;
    }

    private static Bakery.Info calculateSingleRecipe(
            Bakery.Recipe recipe, List<Bakery.Info> inventory) {
        Bakery.Info o = new Bakery.Info();
        int min = 999999999;

        for (Bakery.Info ingredient : recipe.getIngredients()) {
            for (Bakery.Info stock : inventory) {
                if (ingredient.getName().equalsIgnoreCase(stock.getName())) {
                    int currentMin;
                    if (!ingredient.getName().equalsIgnoreCase("egg")) {
                        currentMin =
                            (Integer.parseInt(stock.getAmount().split(" ")[0]) * 1000)
                                / Integer.parseInt(ingredient.getAmount().split(" ")[0]);
                    } else {
                        currentMin =
                            Integer.parseInt(stock.getAmount().split(" ")[0])
                                / Integer.parseInt(ingredient.getAmount().split(" ")[0]);
                    }

                    if (currentMin < min) min = currentMin;
                }
            }
        }

        o.setName(recipe.getName());
        o.setAmount(String.valueOf(min));

        return o;
    }
}
