import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

class Main {
    public static void main(String[] args) {
        System.out.println("Hello world!");

        String filename = "bakery.json";
        JSONObject jsonObject = null;
        try {
            jsonObject = parseJSONFile(filename);
        } catch(Exception ex) {
            System.out.println("ex: " + ex);
        }
        System.out.println("jsonObject: " + jsonObject);
    }

    public static JSONObject parseJSONFile(String filename) throws JSONException, IOException {
        String content = new String(Files.readAllBytes(Paths.get(filename)));
        return new JSONObject(content);
    }
}
